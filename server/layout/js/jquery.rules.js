$(function() {
$("#form").validate({
  rules: {
      loginId: {
          required: true,
          minlength: 6
      },
      loginPass: {
        required: true,
        minlength: 8
      }
      },
  messages: {
      loginId: {
          required: "必須項目です",
          minlength: "英数字組み合わせ6文字以上でご入力ください"
      },
      loginPass: {
          required: "必須項目です",
          minlength: "英数字組み合わせ8文字以上でご入力ください"
      }
      }
  });
});
