<?php
  require 'session.php';

  logout_user();
  header('Location: index.php');
?>
