これ用のDBの作成コマンド。
mysqlを立ち上げてから以下をたたき込む。

CREATE DATABASE copypaste;
use copypaste;
CREATE TABLE users (id int(11) NOT NULL AUTO_INCREMENT, login_id varchar(255) DEFAULT NULL, login_pass text, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE data (id int(11) NOT NULL AUTO_INCREMENT, user_id varchar(255) DEFAULT NULL, url text, title text, content text, date timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ファイル
db_connect.php
のパスワードを修正する。
