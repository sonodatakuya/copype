<?php
    require('session.php');
    require('cache.php');
    require_logined_session();
    require('db_connect.php');

$params = array();
if (isset($_POST['search']) && $_POST['search'] != '') {
   $sql = 'select *, adddate(date, INTERVAL 9 HOUR) as date_jst from data where user_id=? and content REGEXP ? or title regexp ? order by id desc limit 1000;';
   $stmt = $dbh->prepare($sql);
   $stmt->execute(array($_SESSION["user_id"],$_POST['search'],$_POST['search']));
} else {
   $sql = 'select *, adddate(date, INTERVAL 9 HOUR) as date_jst from data where user_id=? order by id desc limit 1000;';
   $stmt = $dbh->prepare($sql);
   $stmt->execute(array($_SESSION["user_id"]));
}

$resultdata = array();
while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
  $resultdata[] = $result;
}
?>
<!DOCTYPE html>
<html lang=ja>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>copy&paste log</title>
<!-- <link rel="shortcut icon" href="favicon.ico" > -->
<!-- <link rel="stylesheet" href="body.css" type="text/css" media="all">-->
<style type="text/css">
<!--
    #sidebar{
        float: left;
        width: 400px;
        height: 200px;
        font-size: small;
        border: 1px #c0c0c0 solid;
        overflow: scroll;
    }
    #main{
        float: right;
        width: 880px;
        height: 400px;
        border: 1px #c0c0c0 solid;
        overflow: scroll;
    }
-->
</style>
</head>
<body>
<div id="sidebar">
        <?php foreach ($resultdata as $data) : ?>
              <div>
                [<?=hx($data['id'])?>] <a href=./top.php#content<?=hx($data['id'])?>> <?=hx($data['date'])?> --  <?=hx($data['title'])?></a> <?=hx($data['content'])?></a>
                <br>
              </div>
        <?php endforeach; ?>
</div>
<form action = "top.php"  method = "post">
<input type  = "text"       name   = "search" value="<?php echo $_POST['search'];?>" autofocus>
<input type  = "submit"     value  = "search" style="text-align: right">
</form>

<a href ="logout.php">ログアウト</a>



<?php
if (isset($_POST['search']) && $_POST['search'] != '') {echo $_POST['search'];}
?>
<div id="main">
     <?php foreach ($resultdata as $data) : ?>
           <div>
                <a name="content<?=hx($data['id'])?>"><?=hx($data['date'])?></a> -- <a href=<?=hx($data['url'])?>><?=hx($data['title'])?></a></a><br><?=nl2br(hx($data['content']))?>
                <br><br>
           </div>
     <?php endforeach; ?>
</div>
</body>
</html>
