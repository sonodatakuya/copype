<?php
require('db_connect.php');
require('session.php');

$login_id = $_POST['loginid'];
$login_pass = $_POST['loginpass']; 
$login_pass2 = $_POST['loginpass2'];

if ($login_pass == $login_pass2){

if ($login_id !='' && $login_pass != '' && $login_pass2 != '') {
            $error_msg = "";
            $dbh->beginTransaction();
            
            $sql = 'select * from users where login_id = ?';
            $stmt = $dbh->prepare($sql);
            $stmt->execute(array($login_id));
    
            if($stmt->fetch(PDO::FETCH_ASSOC)){
                $error_msg = "ログインIDが重複しています";
                header('Location: index2.php?reg_msg=' . $error_msg);
            }
            
            if($error_msg == ""){
                $sql = 'insert into users (login_id, login_pass) value (?, ?)';
                $stmt = $dbh->prepare($sql);
                $stmt->execute(array($login_id, $login_pass));
        
                
                $user_id = $dbh->lastInsertId('id');
                save_user($user_id);
                $dbh->commit();
                header('Location: top.php');
            }else{
                header('Location: index2.php?reg_msg=' . $error_msg);
            } 
}   


}else{
    $error_msg = "パスワードが一致しません";
    header('Location: index2.php?reg_msg=' . $error_msg);
}
?>



