<?php

function require_logined_session(){
    // セッション開始
    session_start();
    // ログインしていなければ /login.php に遷移
    if (!isset($_SESSION['user_id'])) {
        header('Location: /index.php');
        exit;
    }
}
?>
