<?php
    require('db_connect.php');
    require('session.php');

class copypaste
{
    var $dbh;

    function __construct() {
        $this->connect();

        if (isset($_POST['func']) &&  isset($_POST['url']) && isset($_POST['title']) && isset($_POST['content']) ) {
            if ($_POST['func'] == 'add' && $_POST['url'] != '' && $_POST['title'] != '' && $_POST['content'] != ''){
                $this->insertHonbun($_POST['url'], $_POST['title'], $_POST['content'], date('Y-m-d H;i;s'));
            }
        }
        if (isset($_GET['func']) && $_GET['func'] == 'disp') {
            $this->disp();
        } else {
            $this->json();
        }
        if (isset($_GET['func']) && $_GET['func'] == 'disp') {
            $this->disp();
        } else {
            $this->json();
        }
    }
    function connect() {
        $dsn=$GLOBALS['dsn'];
        try{
            $this->dbh = new PDO($GLOBALS['dsn'], $GLOBALS['user'], $GLOBALS['password']);
        }catch (PDOException $e){
            echo('Connection failed:'.$e->getMessage());
            die();
        }
    }
    function getData($category = '', $title_id = '') {
        $params = array();
        $sql = 'select id, url, title, content, date from data where user_id=? order by id desc limit 1000;';

        $stmt = $this->dbh->prepare($sql);
        $stmt->execute(array($_SESSION["user_id"]));

        $results = array();
        while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $results[] = $result;
        }
        return $results;
    }
    function insertHonbun($url, $title, $content, $date) {
        $sql = 'insert into data (url, title, content, date, user_id) value (?, ?, ?, ?, ?)';
        $stmt = $this->dbh->prepare($sql);
        $stmt->execute(array($url, $title, $content, $date, $_SESSION["user_id"]));
    }

    function json() {
        $resultdata = $this->getData();
        echo json_encode($resultdata);
    }

    function template($name, $vars = array()) {
        if (is_file("./" . $name . '.php')) {
            ob_start();
            extract($vars);
            require("./" . $name . '.php');
            $contents = ob_get_contents();
            ob_end_clean();
            return $contents;
        }
        return false;
    }

}

new copypaste();

?>
