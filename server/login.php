<?php

require 'db_connect.php';
require 'session.php';

$login_id = $_POST['loginid'];
$login_pass = $_POST['loginpass']; 

if ($login_id != '' && $login_pass != '') {
    $sql = 'select id from users where (login_id = ? and login_pass = ?)';
    $stmt = $dbh->prepare($sql);
    $stmt->execute(array($login_id, $login_pass));
    
    $user_id = $stmt->fetch(PDO::FETCH_ASSOC)['id'];
    
    if($user_id){
        save_user($user_id);
        header('Location: top.php');
    }else{
        $error_msg = "ログイン情報が間違っています";
        header('Location: index.php?msg=' . $error_msg);
    }
}else{
    header('Location: index.php');
}

?>



